<%@ page import="com.sample.Core.*" %>
<%@ page import="com.sample.Dao" %>

<%
    Core c = Core.getInstance();
    Dao dao = c.getDao();


    if(request.getParameter("user") != null){
        String fullName = dao.getNameByUser(request.getParameter("user"), request.getParameter("pass"));
        String responce = "";
        if(fullName == null){
            responce = "{\"status\":\"failed\"}";
        } else {
            responce = "{ \"name\":\"John\", \"full_name\":\" " + fullName + "\"}";
        }
        out.println(responce);
        return;
    }

%>
<html>
    <head>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    </head>
    <body>
        <div id='content'>
        <h1>Welcome</h1>
            <table>
                <tr>
                    <td>
                        <div>User</div>
                    </td>
                    <td>
                        <div><input type="text" id="user"/></div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div>Password</div>
                    </td>
                    <td>
                        <div><input type="password" id="pass"/></div>
                    </td>
                </tr>
            </table>
            <div>
                <input type="button" value="Submit" onclick="getUserData()"/>
            </div>
        </div>
        <script>
            function getUserData(){
                var user = $("#user").val();
                var pass = $("#pass").val();
                
                $.ajax({
                    url: 'index.jsp?user=' + user + '&pass=' + pass,
                    method: 'GET',
                    success: function(data){
                        var res = JSON.parse(data);
                        if(res.status == 'failed'){
                            $("#content").html("<h1>Wrong Credentials</h1>")
                        } else {
                            $("#content").html("<h1>Wellcome " +  res.full_name + "</h1>")
                        }
                    }
                });
            }
        </script>
    </body>
</html>