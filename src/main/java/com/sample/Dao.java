package com.sample;

import java.sql.*;
import java.security.*;


public class Dao {
    
    Connection conn = null;
    
    public Dao(Connection conn){
        this.conn = conn;
    }
    
    public String getNameByUser(String user, String pass){
        PreparedStatement st = null;
        ResultSet rs = null;
        String result = null;
        try{
            st = conn.prepareStatement("SELECT * FROM users WHERE user = ?");
            st.setString(1, user);
            rs = st.executeQuery();

            if(rs.next()){

                String rsPass = rs.getString("password");
                byte[] bytesOfMessage = pass.getBytes("UTF-8");
                MessageDigest md = MessageDigest.getInstance("MD5");
                if((new String(md.digest(bytesOfMessage)).equalsIgnoreCase(rsPass))){
                    
                }
                result = rs.getString("full_name");
            } else {
                return null;
            }
                               
        } catch (Exception e){
            e.printStackTrace();
        } finally{
            try{st.close();} catch(Exception e){}
            try{rs.close();} catch(Exception e){}
        }
        
        return result;
    }
    
}
