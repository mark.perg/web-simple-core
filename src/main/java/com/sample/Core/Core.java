package com.sample.Core;

import java.security.InvalidParameterException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;
import java.util.Arrays;
import java.util.Comparator;
import java.io.*;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

import com.sample.*;


public class Core {
    
    private String coreName;
    private static Core instance;
    private Connection conn;
    Properties props = new Properties();
    private Dao dao = null;
    
    private Core(){
        this.init();
    }
    
    public synchronized static Core getInstance(){
        if(instance == null){
            instance = new Core();
        }
        return instance;
    }
    
    private void init(){
        this.coreName = "Main Core";
        
        //Reading configuration
        
                   
        
        
        try{
            InputStream stream = getClass().getClassLoader().getResourceAsStream("conf/application.properties");
            props.load(stream);
            
            Class.forName(props.getProperty("db.driver")).newInstance();
            
            conn = DriverManager.getConnection(props.getProperty("db.connection.string").trim() ,
                props.getProperty("db.user").trim(), 
                props.getProperty("db.pass").trim());
                                   

        } catch(Exception e){
            e.printStackTrace();
        }
        
        
        boolean shouldCreateSchema = false;
        try{
           shouldCreateSchema =  Boolean.parseBoolean(props.getProperty("on.load.recreate.schema")); 
        } catch(Exception e){
            e.printStackTrace();
        }
        
        if(shouldCreateSchema){
            
            //First we will create a schema and then we will select it
            
            PreparedStatement st = null;
            try{
                st = conn.prepareStatement("CREATE SCHEMA IF NOT EXISTS " + props.getProperty("schema.name"));
                st.executeUpdate();    

                conn = DriverManager.getConnection(props.getProperty("db.connection.string").trim() +  props.getProperty("schema.name").trim(),
                    props.getProperty("db.user").trim(), 
                    props.getProperty("db.pass").trim());

                 
                                   
            } catch (SQLException e){
                e.printStackTrace();
            } finally{
                try{st.close();} catch(Exception e){}
            }
            
            try{
                conn.setSchema(props.getProperty("schema.name"));
            } catch (Exception e){
                e.printStackTrace();
            }
             
            
            File sqlFolder = new File(getClass().getClassLoader().getResource("sql").getFile());
            if(!sqlFolder.isDirectory()){
                throw new InvalidParameterException("Spefified SQL folder is not a directory!");
            } else {
                File[] sqls= sqlFolder.listFiles();
                Arrays.sort(sqls, new Comparator<File>(){
                    @Override
                    public int compare(File f1, File f2) {
                        return ((File) f1).getName().compareTo(((File) f2).getName());
                    }
                });
                
                for(File f : sqls){
                    BufferedReader br = null;
                    try {
                        br = new BufferedReader(new FileReader(f));
                        StringBuilder sb = new StringBuilder();
                        String line = br.readLine();
                    
                        while (line != null) {
                            if(!line.startsWith("#")){
                                sb.append(line);
                                sb.append(System.lineSeparator());
                                line = br.readLine();
                            }

                        }
                        String query = sb.toString();
                        if(query.trim().length() > 0){
                            try{
                                st = conn.prepareStatement(query);
                                st.executeUpdate();
                            } catch (SQLException e){
                                e.printStackTrace();
                            } finally{
                                try{st.close();} catch(Exception e){}
                            }

                        }
                        
                    } catch(Exception e){
                        e.printStackTrace();
                    } finally {
                        try{br.close();} catch (Exception e){}
                    }
                }
            }
        }
        
        dao = new Dao(conn);
    }
    
    public String getCoreName(){
        return this.coreName;
    }
    
    public Dao getDao(){
        return dao;
    }
    
    public Connection getConnection(){
        return conn;
    }
    
    private void prepareDatabase(){
        
    }
}
