package com.sample.Servlets;

import javax.servlet.http.HttpServlet;

import com.sample.Core.Core;

public class InitCoreServlet extends HttpServlet{
    
    private static final long serialVersionUID = 1L;
    
    public void init(){
        Core c = Core.getInstance();
        System.out.println("Core with the name " + c.getCoreName() + " started!");
    }
}
